#ifndef BASPRG2_NODE_H
#define BASPRG2_NODE_H

struct Node
{
	int data;
	Node* next = NULL;
	Node* previous = NULL;
};
#endif// !BASPRG2_NODE_H 
