#pragma once
#include <string>

using namespace std;
class CloudStrife
{
public:
	CloudStrife();
	CloudStrife(int hp, int mp, int attack, int exp, int defense, int magicAtk, int magicDef);
	int hp;
	int mp;
	int attack;
	int exp;
	int defense;
	int magicAtk;
	int magicDef;
};
