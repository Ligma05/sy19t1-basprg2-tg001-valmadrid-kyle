#include<iostream>
#include "Wizard.h"
#include "Spell.h"

Wizard::Wizard()
{
	this->name = "";
	this->hp = 0;
	this->mp = 0;
}

Wizard::Wizard(string name, int hp, int mp)
{
	this->name = name;
	this->hp = hp;
	this->mp = mp;
}

void Wizard::displaystats(Spell*spell)
{
	cout << "\nPlayer " << this->name << " stats:\n";
	cout << "Health Points: " << this->hp << endl;
	cout << "Mana Points: " << this->mp << endl;
	cout << "Skill: " << spell->name << endl;
	
}

void Wizard::attack(Spell * spell, Wizard * EnemyHp)
{
	EnemyHp->hp -= spell->damage;
	this->mp -= spell->mp;
}


