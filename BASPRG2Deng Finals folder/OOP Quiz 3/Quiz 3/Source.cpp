#include <iostream>
#include <string>
#include "character.h"
#include <time.h>
#include <conio.h>

using namespace std;

int main()
{
	srand(time(NULL));
	string name;
	int choice = 0;
	string type;
	int hitChance = rand() % 50 + 1;
	cout << "Input Your Name: ";
	cin >> name;
	character *player[3];
	player[0] = new character(name,"Wizard", 15, 10, 10, 10, 10);
	player[1] = new character(name, "Mage", 13, 12, 10, 10, 10);
	player[2] = new character(name, "Assassin", 11, 15, 10, 10, 10);
	
	
	while (choice <= 0 || choice > 3) {
		cout << "What Class do you pick: \n";
		cout << "[1] Wizard \n";
		cout << "[2] Mage \n";
		cout << "[3] Assassin \n";
		cin >> choice;
	}
	system("CLS");
	choice = choice - 1;

	int typeRandomizer = rand() % 2;
	character* enemy[3];
	enemy[0] = new character("Enemy Wizard", "Wizard", 15, 10, 10, 10, 10);
	enemy[1] = new character("Enemy Mage", "Mage", 13, 12, 10, 10, 10);
	enemy[2] = new character("Enemy Assassin", "Assassin", 11, 15, 10, 10, 10);

	while (player[choice]->getHP() > 0) {
		//continue game
		
		player[choice]->displaystats();
		cout << "VS \n";
		enemy[typeRandomizer]->displaystats();
		_getch();
		system("CLS");
		//Match commencing
		while (player[choice]->getHP() > 0 && enemy[typeRandomizer]->getHP() > 0) {
			//continue match
			hitChance;
			if (hitChance <= player[choice]->getAgi() && hitChance > enemy[typeRandomizer]->getAgi() ) {
				cout << player[choice]->getName() << " Miss \n";
				_getch();
				cout << enemy[typeRandomizer]->getName() << " Damage " << (player[choice]->getName()) << " by " << player[choice]->hpDamage(enemy[typeRandomizer]->getDamage(player[choice])) << endl;
				_getch();
			}
			else if (hitChance <= enemy[typeRandomizer]->getAgi() && hitChance > player[choice]->getAgi()) {
				cout << player[choice]->getName() << " Damage " << (enemy[typeRandomizer]->getName()) << " by " << enemy[typeRandomizer]->hpDamage(player[choice]->getDamage(enemy[typeRandomizer])) << endl;
				_getch();
				cout << enemy[typeRandomizer]->getName() << " Miss \n";
				_getch();
			}
			else if (hitChance <= enemy[typeRandomizer]->getAgi() && hitChance <= player[choice]->getAgi()) {
				cout << player[choice]->getName() << " Miss \n";
				_getch();
				cout << enemy[typeRandomizer]->getName() << " Miss \n";
				_getch();
			}
			else if (hitChance > enemy[typeRandomizer]->getAgi() && hitChance > player[choice]->getAgi()) {
				cout << player[choice]->getName() << " Damage " << (enemy[typeRandomizer]->getName()) << " by " << enemy[typeRandomizer]->hpDamage(player[choice]->getDamage(enemy[typeRandomizer])) << endl;
				_getch();
				cout << enemy[typeRandomizer]->getName() << " Damage " << (player[choice]->getName()) << " by " << player[choice]->hpDamage(enemy[typeRandomizer]->getDamage(player[choice])) << endl;
				_getch();
			}
			
			if (player[choice]->getHP() <= 0) {
				
				system("CLS");
				cout << "Round has ended \n";
				_getch();
				system("CLS");
				player[choice]->displaystats();
				_getch();
				cout << "You Lose" << endl;
				_getch();
				return 0;
			}
			else if (enemy[typeRandomizer]->getHP() <= 0) {
				cout << "You Won The Round" << endl;
				_getch();
				cout << "Healing process" << endl;
				_getch();
				player[choice]->healingUP();
				cout << "Healing done" << endl;
				_getch();
				system("CLS");
			}
		}
		
	}


	system("pause");
	return 0;
}
