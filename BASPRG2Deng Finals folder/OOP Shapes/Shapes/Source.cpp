#include <iostream>
#include <string>
#include "Shapes.h"
#include <conio.h>

using namespace std;

int main() {
	int choice = 0;
	Shapes* objects[3];
	objects[0] = new Shapes("Circle", 0, 1);
	objects[1] = new Shapes("Square", 4, 6);
	objects[2] = new Shapes("Rectangle", 4, 6);
	
	while (choice != 4) {
		cout << "Choose a Shape to display: \n";
		cout << "[1] = Circle \n";
		cout << "[2] = Square \n";
		cout << "[3] = Rectangle \n";
		cout << "[4] = Exit \n";
		cin >> choice;
		if (choice == 4) {
			return 0;
		}
		else {
			objects[choice - 1]->displayShapes();
		}
	}
	system("pause");
	return 0;
}