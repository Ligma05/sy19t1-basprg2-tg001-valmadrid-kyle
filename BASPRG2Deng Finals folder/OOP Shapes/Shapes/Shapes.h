#pragma once
#include<iostream>
#include <string>

using namespace std;

class Shapes
{
public:
	Shapes(string shape, int sides, int area);
	void displayShapes();
	string getShape();
	int getSides();
	int getArea();
private:
	string mShape;
	int mSides;
	int mArea;
};

