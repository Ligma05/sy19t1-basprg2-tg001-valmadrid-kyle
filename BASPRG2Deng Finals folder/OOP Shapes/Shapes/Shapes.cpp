#include "Shapes.h"




Shapes::Shapes(string shape, int sides, int area)
{
	mShape = shape;
	mSides = sides;
	mArea = area;
}

void Shapes::displayShapes()
{
	cout << "Shape: " << mShape << endl;
	cout << "Sides: " << mSides << endl;
	cout << "Area: " << mArea << endl;
}

string Shapes::getShape()
{
	return mShape;
}

int Shapes::getSides()
{
	return mSides;
}

int Shapes::getArea()
{
	return mArea;
}
