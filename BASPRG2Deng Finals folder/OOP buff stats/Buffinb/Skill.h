#pragma once
#include "Skill.h"
class Unit;
class Skill
{
public:
	Skill(int value);

	virtual void Cast(Unit * target);
protected:

	int mValue;
};

