#pragma once
#include "Skill.h"
#include "Unit.h"
class IronSkin: public Skill
{
public:
	IronSkin(int value);
	virtual void Cast(Unit* target);
};

