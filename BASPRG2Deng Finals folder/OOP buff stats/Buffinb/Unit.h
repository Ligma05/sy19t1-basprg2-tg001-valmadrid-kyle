#pragma once
#include <iostream>
#include <vector>
#include "Skill.h"
#include "Haste.h"
#include "Heal.h"
#include "IronSkin.h"
#include "Might.h"
#include "Concentration.h"
using namespace std;
class Unit
{
public:
	Unit(int hp, int pow, int vit, int dex, int agi);
	int getHp();
	int getPow();
	int getDex();
	int getMagi();
	int getVit();

	void setAgi(int value);
	void setVit(int value);
	void setDex(int value);
	void setPow(int value);
	void healHp(int value);

	void displaystats();
	Skill* getSkill(int index);
private:
	int mhp;
	int mpow;
	int mvit;
	int mdex;
	int magi;
	vector<Skill*> mSkill;
	
};

