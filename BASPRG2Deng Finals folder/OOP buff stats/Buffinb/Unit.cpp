#include "Unit.h"





Unit::Unit(int hp, int pow, int vit, int dex, int agi)
{
	mhp = hp;
	mpow = pow;
	mvit = vit;
	mdex = dex;
	magi = agi;

	mSkill.push_back(new Heal(10));
	mSkill.push_back(new IronSkin(2));
	mSkill.push_back(new Haste(2));
	mSkill.push_back(new Concentration(2));
	mSkill.push_back(new Might(2));

}

int Unit::getHp()
{
	return mhp;
}

int Unit::getPow()
{
	return mpow;
}

int Unit::getDex()
{
	return mdex;
}

int Unit::getMagi()
{
	return magi;
}

int Unit::getVit()
{
	return mvit;
}

void Unit::setAgi(int value)
{
	magi = value;
}

void Unit::setVit(int value)
{
	mvit = value;
}

void Unit::setDex(int value)
{
	mdex = value;
}

void Unit::setPow(int value)
{
	mpow = value;
}

void Unit::healHp(int value)
{
	mhp = value;
}

void Unit::displaystats()
{
	cout << "Health: " << mhp;
	cout << "Power: " << mpow;
	cout << "Dexterity: " << mdex;
	cout << "Vitality: " << mvit;
	cout << "Agility: " << magi;
}

Skill* Unit::getSkill(int index)
{
	return mSkill[index];
}
