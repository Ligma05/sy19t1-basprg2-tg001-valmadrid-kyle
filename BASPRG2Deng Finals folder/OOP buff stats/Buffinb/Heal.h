#pragma once
#include "Skill.h"
#include "Unit.h"
class Heal: public Skill
{
public:
	Heal(int value);

	virtual void Cast(Unit* target);
};

