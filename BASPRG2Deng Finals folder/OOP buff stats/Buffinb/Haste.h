#pragma once
#include "Skill.h"
#include "Unit.h"
class Haste : public Skill
{
public:
	Haste(int value);
	
	virtual void Cast(Unit* target);
};

