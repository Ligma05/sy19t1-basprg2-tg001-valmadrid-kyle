#pragma once
#include "Skill.h"
#include "Unit.h"
class Might: public Skill
{
public:
	Might(int value);
	
	virtual void Cast(Unit* target);

};

