#pragma once
#include <string>

using namespace std;
class CloudStrife
{
public:
	CloudStrife();
	CloudStrife(int hp, int mp, int attack, int defense, int magicAtk, int magicDef, string Wpn, string Arm, string Acc);
	void displaystat();
private:
	int hp;
	int mp;
	int attack;
	int defense;
	int magicAtk;
	int magicDef;
	string Arm;
	string Acc;
	string Wpn;
};
