#include <iostream>
#include "CloudStrife.h"

CloudStrife::CloudStrife()
{
	this->hp = 0;
	this->attack = 0;
	this->magicAtk = 0;
	this->mp = 0;
	this->magicDef = 0;
	this->defense = 0;
	this->Acc = "";
	this->Arm = "";
	this->Wpn = "";
}

CloudStrife::CloudStrife(int hp, int mp, int attack, int defense, int magicAtk, int magicDef, string Wpn, string Arm, string Acc)
{
	this->hp = hp;
	this->mp = mp;
	this->attack = attack;
	this->defense = defense;
	this->magicAtk = magicAtk;
	this->magicDef = magicDef;
	this->Acc = Acc;
	this->Arm = Arm;
	this->Wpn = Wpn;
}

void CloudStrife::displaystat()
{
	cout << "||CloudStrife|| \n";
	cout << "HP: " << this->hp << endl;
	cout << "MP: " << this->mp << endl;
	cout << "Attack: " << this->attack << endl;
	cout << "Defense: " << this->defense << endl;
	cout << "magicAtk: " << this->magicAtk << endl;
	cout << "magicDef: " << this->magicDef << endl;
	cout << endl;
	cout << "Wpn: " << this->Wpn << endl;
	cout << "Acc: " << this->Acc << endl;
	cout << "Arm: " << this->Arm << endl;
}
