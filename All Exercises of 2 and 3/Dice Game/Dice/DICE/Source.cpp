#include <iostream>
#include <time.h>

using namespace std;

int diceroll(int &dice1, int &dice2) {

	dice1 = rand()%6 +1;
	dice2 = rand() % 6 + 1;
	int Total = dice1 + dice2;
	return Total;
}
void display(int dice1, int dice2) {
	cout << "[" << dice1 << ", " << dice2 << "]";
}


int main() {
	srand(time(NULL));
	int pd1;
	int pd2;
	int ed1;
	int ed2;

	if (diceroll(pd1, pd2) < diceroll(ed1, ed2)) {
		display(pd1, pd2);
		display(ed1, ed2);
		cout << "\n you lost the round \n";
		system("pause");
		
	}
	else if (diceroll(pd1, pd2) > diceroll(ed1, ed2)) {
		display(pd1, pd2);
		display(ed1, ed2);
		cout << "\n you won the round \n";
		system("pause");
	}
	else if (diceroll(pd1, pd2) == diceroll(ed1, ed2)) {
		display(pd1, pd2);
		display(ed1, ed2);
		cout << "Draw \n";
		system("pause");
	}
	
}