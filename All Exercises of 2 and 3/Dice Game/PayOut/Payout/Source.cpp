#include <iostream>
#include <string>

using namespace std;

int diceroll(int &dice1, int &dice2) { // value of dice

	dice1 = rand() % 6 + 1;
	dice2 = rand() % 6 + 1;
	int Total = dice1 + dice2;
	return Total;
}

void display(int dice1, int dice2) { // displays dice
	cout << "[" << dice1 << ", " << dice2 << "]";
}

int betCondition(int bet, int &currentGold) { // deducts gold from bet
	int pd1;
	int pd2;
	int ed1;
	int ed2;
	while (currentGold > 0)
	{
		cout << "You currently have " << currentGold << endl;

		cout << "Enter the amount of your bet: ";
		cin >> bet;

		if (bet <= currentGold && bet > 0)
		{
			currentGold = currentGold - bet;
			if (diceroll(pd1, pd2) < diceroll(ed1, ed2)) {
				display(pd1, pd2);
				display(ed1, ed2);
				cout << "\n you lost the round \n";
				system("pause");

			}
			else if (diceroll(pd1, pd2) > diceroll(ed1, ed2)) {
				currentGold = currentGold + bet * 2;
				display(pd1, pd2);
				display(ed1, ed2);
				cout << "\n you won the round \n";
				
				system("pause");
			}
			else if (diceroll(pd1, pd2) == diceroll(ed1, ed2)) {
				currentGold = currentGold + bet;
				display(pd1, pd2);
				display(ed1, ed2);
				cout << "Draw \n";
				system("pause");
			}
			

			system("pause");
			system("CLS");


		}
		cout << "your have: " << currentGold << " left \n";


	}
	
	system("pause");
	system("CLS");
	return currentGold;
}

int main() {
	int betMoney = 0;
	int currentGold = 1000;
	betCondition(betMoney, currentGold);
	if (currentGold <= 0) {
		cout << "\n Game Over No Gold Left \n";
	}
	system("pause");
	return 0;
}