#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void assignRandomValues(int* value)
{
	for (int i = 0; i < 10; i++)
	{
		*(value + i) = rand() % 99 + 1;
	}
}

void display(int* value)
{
	for (int i = 0; i < 10; i++)
	{
		cout << *(value + i) << endl;
	}
}
void main()
{
	srand(time(NULL));

	int Array[10] = {};
	int* APointer = *&Array;

	assignRandomValues(Array);
	display(Array);

	system("pause");
}