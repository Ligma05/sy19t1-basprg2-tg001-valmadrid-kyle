#include "Bomb.h"
#include "Items.h"
#include "Unit.h"
Bomb::Bomb(string name): Items(name)
{
}

Bomb::~Bomb()
{
}

void Bomb::effect(Unit * player)
{
	player->damageHp(25);
	player->bombCount(1);
}


