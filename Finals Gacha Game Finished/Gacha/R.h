#pragma once
#include <iostream>
#include <string>
#include "Items.h"
using namespace std;
class Unit;
class R : public Items
{
public:
	R(string name);
	~R();
	virtual void effect(Unit* rp);

};

