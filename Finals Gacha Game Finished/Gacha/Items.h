#pragma once
#include <string>
using namespace std;
class Unit;
class Items
{
public:
	Items(string name);
	~Items();
	virtual void effect(Unit*unit);
	string getName();
private:
	string mname;
};

