#include "SSR.h"
#include "Items.h"
#include "Unit.h"
SSR::SSR(string name) : Items(name)
{
}

SSR::~SSR()
{
}

void SSR::effect(Unit * player)
{
	player->addRp(50);
	player->ssrCount(1);
}
