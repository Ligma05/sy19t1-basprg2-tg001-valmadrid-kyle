#pragma once
#include <iostream>
#include <string>
#include "Items.h"
using namespace std;
class Unit;
class Heal: public Items
{
public:
	Heal(string name);
	~Heal();
	virtual void effect(Unit*Health);
};

