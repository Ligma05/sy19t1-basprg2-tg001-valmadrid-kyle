#pragma once
#include <iostream> 
#include <string>
#include <vector>
#include "Items.h"
using namespace std;
class Items;
class Unit
{
public:
	Unit(int hp, int crystals, int rarityPoints, int pull, int hPotion, int bomb, int Tcrystals, int r, int sr, int ssr);
	~Unit();
	int getHP();
	int getCrystals();
	int getRP();
	int getPull();
	int totalHealthPotion();
	int totalBomb();
	int totalCrystal();
	int totalR();
	int totalSR();
	int totalSSR();
	void display();
	void displayItems();
	void addCrystal(int value);
	void addRp(int value);
	void addHealth(int value);
	void damageHp(int value);
	void hpCount(int value);
	void bombCount(int value);
	void crystalCount(int value);
	void rCount(int value);
	void srCount(int value);
	void ssrCount(int value);
	int pullCount();
private:
	int mhp;
	int mcrystals;
	int mrarityPoints;
	int mpull;
	int healthPotion;
	int mBomb;
	int mR;
	int mSR;
	int mSSR;
	int mCrys;
};

