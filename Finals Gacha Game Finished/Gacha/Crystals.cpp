#include "Crystals.h"
#include "Items.h"
#include "Unit.h"
Crystals::Crystals(string name) : Items(name)
{
}

Crystals::~Crystals()
{
}

void Crystals::effect(Unit * player)
{
	player->addCrystal(15);
	player->crystalCount(1);
}


