#pragma once
#include <iostream>
#include <string>
#include "Items.h"
#include "Unit.h"
using namespace std;
class Unit;
class Crystals : public Items
{
public:
	Crystals(string name);
	~Crystals();

	virtual void effect(Unit* cryst);
};

