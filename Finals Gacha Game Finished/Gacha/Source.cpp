#include <iostream>
#include <string>
#include "Unit.h"
#include <conio.h>
#include "Machine.h"
#include "Items.h"
#include <time.h>
using namespace std;

int main() {
	srand(time(NULL));
	Unit * player = new Unit(100, 100, 0, 0, 0, 0, 0, 0, 0, 0);
	Machine* Gacha = new Machine();
	
	while (player->getHP() > 0 || player->getCrystals() > 0) {
		// continue
		player->display();
		_getch();
		Gacha->Roll(player);
		player->pullCount();
		_getch();
		system("ClS");
		if (player->getHP() <= 0 || player->getCrystals() <= 0) {
			cout << "Game over \n";
			cout << "you lose! \n";
			player->display();
			player->displayItems();
			_getch();
			return 0;
		}
		else if (player->getRP() >= 100) {
			cout << "Game Over \n";
			cout << "You win! \n";
			player->display();
			player->displayItems();
			_getch();
			return 0;
		}
	}
	system("pause");
	return 0;
}