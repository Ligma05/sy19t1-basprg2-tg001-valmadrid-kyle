#include "Unit.h"




Unit::Unit(int hp, int crystals, int rarityPoints, int pull, int hPotion, int bomb, int Tcrystals, int r, int sr, int ssr)
{
	mhp = hp;
	mcrystals = crystals;
	mrarityPoints = rarityPoints;
	mpull = pull;
	healthPotion = hPotion;
	mBomb = bomb;
	mCrys = Tcrystals;
	mR = r;
	mSR = sr;
	mSSR = ssr;
}

Unit::~Unit()
{
}


int Unit::getHP()
{
	return mhp;
}

int Unit::getCrystals()
{
	return mcrystals;
}

int Unit::getRP()
{
	return mrarityPoints;
}

int Unit::getPull()
{
	return mpull;
}

int Unit::totalHealthPotion()
{
	return healthPotion;
}

int Unit::totalBomb()
{
	return mBomb;
}

int Unit::totalCrystal()
{
	return mCrys;
}

int Unit::totalR()
{
	return mR;
}

int Unit::totalSR()
{
	return mSR;
}

int Unit::totalSSR()
{
	return mSSR;
}


void Unit::display()
{
	cout << "Health Points: " << mhp << endl;
	cout << "Crystals: " << mcrystals << endl;
	cout << "Rarity Points: " << mrarityPoints << endl;
	cout << "Pulls: " << mpull << endl;
}

void Unit::displayItems()
{
	cout << endl << endl << endl;
	cout << "Items Pulled: \n";
	cout << endl;
	cout << "==================================" << endl << endl;
	cout << "Health Potion x" << healthPotion << endl;
	cout << "Bomb x" << mBomb << endl;
	cout << "Crystals x" << mCrys << endl;
	cout << "SR x" << mSR << endl;
	cout << "R x" << mR << endl;
	cout << "SSR x"<< mSSR << endl;
}


void Unit::addCrystal(int value)
{
	mcrystals += value;
}

void Unit::addRp(int value)
{
	mrarityPoints += value;
}

void Unit::addHealth(int value)
{
	mhp += value;
}

void Unit::damageHp(int value)
{
	mhp -= value;
}

void Unit::hpCount(int value)
{
	healthPotion += value;
}

void Unit::bombCount(int value)
{
	mBomb += value;
}

void Unit::crystalCount(int value)
{
	mCrys += value;
}

void Unit::rCount(int value)
{
	mR += value;
}

void Unit::srCount(int value)
{
	mSR += value;
}

void Unit::ssrCount(int value)
{
	mSSR += value;
}



int Unit::pullCount()
{
	mpull +=1;
	mcrystals -= 5;
	return mpull;
}




