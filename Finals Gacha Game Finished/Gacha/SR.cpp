#include "SR.h"
#include "Items.h"
#include "Unit.h"
SR::SR(string name) : Items(name)
{
}

SR::~SR()
{

}

void SR::effect(Unit * player)
{
	player->addRp(10);
	player->srCount(1);
}
