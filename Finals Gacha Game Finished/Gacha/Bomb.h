#pragma once
#include <iostream>
#include <string>
#include "Items.h"
using namespace std;
class Unit;
class Bomb : public Items
{
public:
	Bomb(string name);
	~Bomb();
	virtual void effect(Unit* Health);
};

