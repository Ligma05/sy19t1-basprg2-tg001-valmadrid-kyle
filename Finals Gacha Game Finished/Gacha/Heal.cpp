#include "Heal.h"
#include "Items.h"
#include "Unit.h"
Heal::Heal(string name) : Items(name)
{
}

Heal::~Heal()
{
}

void Heal::effect(Unit * player)
{
	player->addHealth(30);
	player->hpCount(1);
}


