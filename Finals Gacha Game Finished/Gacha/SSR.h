#pragma once
#include <iostream>
#include <string>
#include "Items.h"
using namespace std;
class Unit;
class SSR : public Items
{
public:
	SSR(string name);
	~SSR();
	
	virtual void effect(Unit* player);
};

