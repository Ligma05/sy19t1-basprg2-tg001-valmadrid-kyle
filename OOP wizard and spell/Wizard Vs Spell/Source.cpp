#include <iostream>
#include "Spell.h"
#include "Wizard.h"
#include <time.h>
using namespace std;

int main()
{
	srand(time(NULL));
	int randomDamage = rand() % 30 + 1;
	Wizard* player = new Wizard("Ice king", 100, 100);
	Wizard* enemy = new Wizard("Ice queen", 100, 100);
	Spell*skill = new Spell("SnowBall", 10, randomDamage);


	while (player->hp != 0 || enemy->hp != 0) {
		//continue
		player->attack(skill, enemy);
		enemy->attack(skill, enemy);
		player->displaystats(skill);
		enemy->displaystats(skill);
		system("pause");
		system("CLS");
		if (player->hp <= 0 || enemy->mp <=0) {
			cout << enemy->name << "WINS" << endl;
			system("pause");
			return 0;
		}
		else if (enemy->hp <= 0 || enemy->mp <= 0) {
			cout << player->name << "wins" << endl;
			system("pause");
			return 0;
		}
	}
	

}