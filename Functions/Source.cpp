#include <iostream>
#include <string>


using namespace std;


int Factorial(int x) {
	int factorial = 1;
	if ((x == 0) || (x == 1)) {
		cout << factorial << endl;
	}
	else if (x < 0) {
		cout << "invalid" << endl;
		return 0;
	}
	else if (x > 0) {
		for (int i = 1; i <= x; ++i)
		{
			factorial = factorial * i;
		}
		cout << "factorial of "<< x << "= " << factorial << endl;
	}
	
	return factorial;
}
int main() {
	
	int num;
	cout << "what num do u want to compute? " << endl;
	cin >> num;
	Factorial(num);
	system("Pause");
	return '0';
}